require File.dirname(__FILE__) + '/spec_helper'

describe YAMLConfig do

  it 'takes a path and filename in the initializer'

  it 'has a filename'

  it 'returns the yaml data as a hash'

  it 'raises an error if #read is not overridden'

  it 'raises an error if #to_s is not overridden'

end
