#! /usr/bin/env ruby
require 'rubygems'
require 'elif'

# last_line = `tail -n 1 #{file_name}`

last_line = Elif.open('output.txt') { |f| f.gets }

puts "The last line is #{last_line}!"
