# This class processes a YAML file of Handbrake Options
# and can return it as a properly formatted string
#
class HBConfig < YAMLConfig
  attr_accessor :options

  def read
    @data ||= open(filename) { |f| YAML.load f }

    if @data['video'].keys.include?('x264opts')
      @data['video']['x264opts'] = @data['video']['x264opts'].map {|k,v| "#{k}=#{v}"}.join(':') 
    end

    loose_anamorphic = @data['video'].delete 'loose-anamorphic'
    @anamorphic_options = if loose_anamorphic and loose_anamorphic == true
      ' -loose-anamorphic'
    elsif loose_anamorphic
      " -loosePixelratio=#{ loose_anamorphic }"
    else
      ''
    end

    @options = @data
    self.to_s
  end


  def to_s
    @options.map do |o| 
      o.last.map do |k,v| 
        "--#{k} #{v.inspect}"
      end.join(' ')
    end.join(' ').gsub('true', '') + @anamorphic_options
  end

end
