# This class processes a yaml file queue of items
# with handbrake, using the given HBConfig configuration
#
class HBQueue < YAMLConfig
  attr_accessor :queue

  def executable
    'HandBrakeCLI'
  end

  def read
    @queue ||= open(filename) { |f| YAML.load f }
    puts "#{queue.size} item(s) in the queue."
  end

  def run
    queue.each do |q|
      markers = q.delete 'markers'

      q['input']  = File.expand_path q['input']  if q['input']
      q['output'] = File.expand_path q['output'] if q['output']

      item_options = q.map { |k,v| "--#{k} #{v.inspect}" }.join(' ').gsub('true', '')

      if markers and markers == true
        item_options += ' --markers'
      elsif markers
        item_options += " --markers=#{ markers }"
      end

      execute "#{ executable } #{ item_options } #{ @options }"
    end
  end

private

  def execute command
    puts "\nExecuting:\n\n#{ command }\n\n"

    IO.popen(command) do |output|
      begin
        puts line while line = output.gets
      rescue
        puts 'Finished!'
      end
    end

    10.times { puts }
  end

end
