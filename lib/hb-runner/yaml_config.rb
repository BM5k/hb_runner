require 'yaml'

# this class DRYs up the yaml processing code
# for both HBQueue and HBConfig
#
class YAMLConfig
  attr_accessor :file

  def initialize options = {} 
    options.each { |k,v| instance_variable_set "@#{k}", v }
    read if @file
  end

  def filename() File.expand_path file end

  def read() raise 'Override!' end
  def to_s() raise 'Override!' end

end
